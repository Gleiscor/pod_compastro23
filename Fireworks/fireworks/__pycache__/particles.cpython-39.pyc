a
    �'e�  �                   @  s<   d Z ddlmZ ddlZddlmZ dgZG dd� d�Z	dS )a   
==============================================================
Particles data structure , (:mod:`fireworks.particles`)
==============================================================

This module contains the class used to store the Nbody particles data


�    )�annotationsN�	Particlesc                   @  s�   e Zd ZdZdddd�dd�Zdd�dd�Zdd	�d
d�Zdd	�dd�Zdd	�dd�Zdd	�dd�Z	dd	�dd�Z
d'ddd�dd�Zd(ddd�dd�Zd d	�dd�Zdd	�dd �Zd!d	�d"d#�Zd!d	�d$d%�Zd&S ))r   a  
    Simple class to store the properties position, velocity, mass of the particles.
    Example:

    >>> from fireworks.particles import Particles
    >>> position=np.array([[1.,1.,1.],[2.,2.,2.],[3.,3.,3.]])
    >>> velocity=np.array([[0.,0.,0.],[0.,0.,0.],[0.,0.,0.]])
    >>> mass=np.array([1.,1.,1.])
    >>> P=Particles(position,velocity,mass)
    >>> P.pos # particles'positions
    >>> P.vel # particles'velocities
    >>> P.mass # particles'masses
    >>> P.ID # particles'unique IDs

    The class contains also methods to estimate the radius of all the particles (:func:`~Particles.radius`),
    the module of the velociy of all the particles (:func:`~Particles.vel_mod`), and the module the positition and
    velocity of the centre of mass (:func:`~Particles.com_pos` and :func:`~Particles.com_vel`)

    >>> P.radius() # return a Nx1 array with the particle's radius
    >>> P.vel_mod() # return a Nx1 array with the module of the particle's velocity
    >>> P.com() # array with the centre of mass position (xcom,ycom,zcom)
    >>> P.com() # array with the centre of mass velocity (vxcom,vycom,vzcom)

    It is also possibile to set an acceleration for each particle, using the method set_acc
    Example:

    >>> acc= some_method_to_estimate_acc(P.position)
    >>> P.set_acc(acc)
    >>> P.acc # particles's accelerations

    Notice, if never initialised, P.acc is equal to None

    The class can be used also to estimate the total, kinetic and potential energy of the particles
    using the methods :func:`~Particles.Etot`, :func:`~Particles.Ekin`, :func:`~Particles.Epot`
    **NOTICE:** these methods need to be implemented by you!!!

    The method :func:`~Particles.copy` can be used to be obtaining a safe copy of the current
    Particles instances. Safe means that changing the members of the copied version will not
    affect the members or the original instance
    Example

    >>> P=Particles(position,velocity,mass)
    >>> P2=P.copy()
    >>> P2.pos[0] = np.array([10,10,10]) # P.pos[0] will not be modified!

    znpt.NDArray[np.float64])�position�velocity�massc                 C  s�   t �|�| _| jjd dkr.td| jj� �� t �|�| _| jjd dkr\td| jj� �� t| j�t| j�krxtd� t �|�| _t| j�t| j�kr�td� t �	t| j��| _
d| _dS )a�  
        Class initialiser.
        It assigns the values to the class member pos, vel, mass and ID.
        ID is just a sequential integer number associated to each particle.

        :param position: A Nx3 numpy array containing the positions of the N particles
        :param velocity: A Nx3 numpy array containing the velocity of the N particles
        :param mass: A Nx1 numpy array containing the mass of the N particles
        �   �   z<Input position should contain a Nx3 array, current shape is z<Input velocity should contain a Nx3 array, current shape is zBPosition and velocity in input have not the same number of elemntsz>Position and mass in input have not the same number of elemntsN)�np�
atleast_2d�pos�shape�print�vel�len�
atleast_1dr   �arange�ID�acc)�selfr   r   r   � r   �p/Users/giulianoiorio/Dropbox/Didattica/Computational_Astro_2324/pod_compastro23/Fireworks/fireworks/particles.py�__init__?   s    ""zParticles.__init__)�accelerationc                 C  s2   t �|�}|jd dkr(td|j� �� || _dS )z�
        Set the particle's acceleration

        :param acceleration: A Nx3 numpy array containing the acceleration of the N particles
        r   r   z@Input acceleration should contain a Nx3 array, current shape is N)r	   r
   r   r   r   )r   r   r   r   r   r   �set_accX   s    
zParticles.set_acc)�returnc                 C  s   t �t j| j| j dd��S )z�
        Estimate the particles distance from the origin of the frame of reference.

        :return:  a Nx1 array containing the particles' distance from the origin of the frame of reference.
        r   ��axis)r	   �sqrt�sumr   �r   r   r   r   �radiusd   s    zParticles.radiusc                 C  s   t �t j| j| j dd��S )z�
        Estimate the module of the velocity of the particles

        :return: a Nx1 array containing the module of the particles's velocity
        r   r   )r	   r   r   r   r   r   r   r   �vel_modm   s    zParticles.vel_modc                 C  s$   t j| j| jj dd�t �| j� S )z�
        Estimate the position of the centre of mass

        :return: a numpy  array with three elements corresponding to the centre of mass position
        r   r   )r	   r   r   r   �Tr   r   r   r   �com_posv   s    zParticles.com_posc                 C  s$   t j| j| jj dd�t �| j� S )z�
        Estimate the velocity of the centre of mass

        :return: a numpy  array with three elements corresponding to centre of mass velocity
        r   r   )r	   r   r   r   r"   r   r   r   r   �com_vel   s    zParticles.com_vel�floatc                 C  s   t d��dS )z�
        Estimate the total potential energy of the particles:
        Ekin=0.5 sum_i mi vi*vi

        :return: total kinetic energy
        �!Ekin method still not implementedN��NotImplementedErrorr   r   r   r   �Ekin�   s    	zParticles.Ekin�        )�	softeningr   c                 C  s   t d��dS )a  
        Estimate the total potential energy of the particles:
        Epot=-0.5 sumi sumj mi*mj / sqrt(rij^2 + eps^2)
        where eps is the softening parameter

        :param softening: Softening parameter
        :return: The total potential energy of the particles
        r&   Nr'   )r   r+   r   r   r   �Epot�   s    zParticles.Epotztuple[float, float, float]c                 C  s&   | � � }| j|d�}|| }|||fS )a  
        Estimate the total  energy of the particles: Etot=Ekintot + Epottot

        :param softening: Softening parameter
        :return: a tuple with

            - Total energy
            - Total kinetic energy
            - Total potential energy
        )r+   )r)   r,   )r   r+   r)   r,   �Etotr   r   r   r-   �   s    zParticles.Etotc                 C  s`   t t�| j�t�| j�t�| j��}| jdur<t�| j�|_t t�| j�t�| j�t�| j��S )ze
        Return a copy of this Particle class

        :return: a copy of the Particle class
        N)r   r	   �copyr   r   r   r   )r   �parr   r   r   r.   �   s    $zParticles.copy�intc                 C  s
   t | j�S )z�
        Special method to be called when  this class is used as argument
        of the Python built-in function len()
        :return: Return the number of particles
        )r   r   r   r   r   r   �__len__�   s    zParticles.__len__�strc                 C  s   d| � � � �S )z�
        Special method to be called when  this class is used as argument
        of the Python built-in function print()
        :return: short info message
        z5Instance of the class Particles
Number of particles: )r1   r   r   r   r   �__str__�   s    zParticles.__str__c                 C  s   | � � S )N)r3   r   r   r   r   �__repr__�   s    zParticles.__repr__N)r*   )r*   )�__name__�
__module__�__qualname__�__doc__r   r   r    r!   r#   r$   r)   r,   r-   r.   r1   r3   r4   r   r   r   r   r      s   .						)
r8   �
__future__r   �numpyr	   �numpy.typing�typing�npt�__all__r   r   r   r   r   �<module>   s
   	